#include "stdafx.h"
#include "problemsettings.h"
using namespace std;

#define _MSC_VER

ProblemSettings::ProblemSettings() { }
ProblemSettings::ProblemSettings(char* settingsFileName)
{
	setProblemSettingsFile(settingsFileName);
}

ProblemSettings::~ProblemSettings(void)
{
	delete[] problemSettingsFile;
}

/* ---------------------------------------------------- */
/* this function reads the parameters that define the	*/
/* benchmark.											*/
/* input: none.										*/
/* return: none.										*/
/* ---------------------------------------------------- */
void ProblemSettings::readSettings()
{
	char comment[200];
	char benchmark[200];
	char stop[200];
	int parameters;
	float threshold;
	ifstream inFile;

#ifdef _MSC_VER
	inFile.open(getProblemSettingsFile());
#else
	inFile.open(getDataSettingsFile());
#endif
	if(!inFile)
	{
		cerr<<"can' t open data settings file!\n";
		exit(1);
	}

	inFile >> stop;
	inFile.getline(comment,200);
	inFile >> threshold;
	inFile.getline(comment,200);
	inFile >> benchmark;
	inFile.getline(comment,200);
	inFile >> parameters;
	inFile.getline(comment,200);

	inFile.close();

	if(strcmp(stop,"fixedCycles")==0)
	{
		setStoppingCriterion(fixedCycles);
	}
	else if(strcmp(stop,"fitness")==0)
	{
		setStoppingCriterion(fitness);
	}
	setAcceptableFitness(threshold);
	if(strcmp(benchmark,"ackley")==0)
	{
		setFunction(ackley);
		setInputs(parameters);
	}
	else if(strcmp(benchmark,"easom")==0)
	{
		setFunction(easom);
		setInputs(2);
	}
	else if(strcmp(benchmark,"goldstein_price")==0)
	{
		setFunction(goldstein_price);
		setInputs(2);
	}
	else if(strcmp(benchmark,"griewank")==0)
	{
		setFunction(griewank);
		setInputs(parameters);
	}
	else if(strcmp(benchmark,"hypersphere")==0)
	{
		setFunction(hypersphere);
		setInputs(parameters);
	}
	else if(strcmp(benchmark,"langermann")==0)
	{
		setFunction(langermann);
		setInputs(parameters);
	}
	else if(strcmp(benchmark,"martin_gaddy")==0)
	{
		setFunction(martin_gaddy);
		setInputs(2);
	}
	else if(strcmp(benchmark,"michalewicz")==0)
	{
		setFunction(michalewicz);
		setInputs(parameters);
	}
	else if(strcmp(benchmark,"rastrigin")==0)
	{
		setFunction(rastrigin);
		setInputs(parameters);
	}
	else if(strcmp(benchmark,"rosenbrock")==0)
	{
		setFunction(rosenbrock);
		setInputs(parameters);
	}
	else if(strcmp(benchmark,"schaffer")==0)
	{
		setFunction(schaffer);
		setInputs(2);
	}
	else if(strcmp(benchmark,"schwefel")==0)
	{
		setFunction(schwefel);
		setInputs(parameters);
	}
	else if(strcmp(benchmark,"shekel")==0)
	{
		setFunction(shekel);
		setInputs(10);
	}
	else if(strcmp(benchmark,"steps")==0)
	{
		setFunction(steps);
		setInputs(parameters);
	}
}

void ProblemSettings::setStoppingCriterion(terminatingCondition criterion)
{
	stoppingCriterion=criterion;
}

terminatingCondition ProblemSettings::getStoppingCriterion()
{
	return stoppingCriterion;
}

void ProblemSettings::setFunction(benchmarkFunction benchmark)
{
	function=benchmark;
}

benchmarkFunction ProblemSettings::getFunction()
{
	return function;
}

void ProblemSettings::setInputs(int features)
{
	inputs=features;
}

int ProblemSettings::getInputs()
{
	return inputs;
}

void ProblemSettings::setProblemSettingsFile(char* fileName)
{
	problemSettingsFile=new char[strlen(fileName)+1];

	strcpy(problemSettingsFile, fileName);
}

char* ProblemSettings::getProblemSettingsFile()
{
	return problemSettingsFile;
}

void ProblemSettings::setAcceptableFitness(float fitness)
{
	acceptableFitness=fitness;
}

float ProblemSettings::getAcceptableFitness()
{
	return acceptableFitness;
}

