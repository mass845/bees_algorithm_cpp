#include "stdafx.h"
#include "beesettings.h"
using namespace std;

BeeSettings::BeeSettings(char* settingsFileName)
{
	setBeeSettingsFile(settingsFileName);
}

BeeSettings::BeeSettings(int n,	int e, int m,	int ne,	int nb,	int cycles, float ngh){
	setScoutBees(n);
	setEliteSites(e);
	setBestSites(m);
	setRecruitedEliteSites(ne);
	setRecruitedBestSites(nb);
	setNeighbourhoodSize(ngh);
	setStagnationLimit(cycles);
}
BeeSettings::~BeeSettings(void)
{
	delete[] beeSettingsFile;
}

/* ---------------------------------------------------- */
/* this function reads the learning parameters of the	*/
/* Bees Algorithm.										*/
/* input: none.											*/
/* return: none.										*/
/* ---------------------------------------------------- */
void BeeSettings::readSettings()
{
	char comment[200];
	int n;
	int e;
	int m;
	int ne;
	int nb;
	int cycles;
	float ngh;
	ifstream inFile;

	inFile.open(getBeeSettingsFile());
	if(!inFile)
	{
		cerr<<"can' t open bee settings file!\n";
		exit(1);
	}

	inFile >> n;
	inFile.getline(comment,200);
	inFile >> e;
	inFile.getline(comment,200);
	inFile >> m;
	inFile.getline(comment,200);
	inFile >> ne;
	inFile.getline(comment,200);
	inFile >> nb;
	inFile.getline(comment,200);
	inFile >> ngh;
	inFile.getline(comment,200);
	inFile >> cycles;
	inFile.getline(comment,200);
	inFile.close();

	setScoutBees(n);
	setEliteSites(e);
	setBestSites(m);
	setRecruitedEliteSites(ne);
	setRecruitedBestSites(nb);
	setNeighbourhoodSize(ngh);
	setStagnationLimit(cycles);
}

void BeeSettings::copySettings(BeeSettings* origin)
{
	setScoutBees(origin->getScoutBees());
	setEliteSites(origin->getEliteSites());
	setBestSites(origin->getBestSites());
	setRecruitedEliteSites(origin->getRecruitedEliteSites());
	setRecruitedBestSites(origin->getRecruitedBestSites());
	setNeighbourhoodSize(origin->getNeighbourhoodSize());
	setStagnationLimit(origin->getStagnationLimit());
}

void BeeSettings::setScoutBees(int bees)
{
	scoutBees=bees;
}

int BeeSettings::getScoutBees()
{
	return scoutBees;
}

void BeeSettings::setEliteSites(int sites)
{
	eliteSites=sites;
}

int BeeSettings::getEliteSites()
{
	return eliteSites;
}

void BeeSettings::setBestSites(int sites)
{
	bestSites=sites;
}

int BeeSettings::getBestSites()
{
	return bestSites;
}

void BeeSettings::setRecruitedEliteSites(int bees)
{
	recruitedEliteSites=bees;
}

int BeeSettings::getRecruitedEliteSites()
{
	return recruitedEliteSites;
}

void BeeSettings::setRecruitedBestSites(int bees)
{
	recruitedBestSites=bees;
}

int BeeSettings::getRecruitedBestSites()
{
	return recruitedBestSites;
}

void BeeSettings::setStagnationLimit(int cycles)
{
	stagnationLimit=cycles;
}

int BeeSettings::getStagnationLimit()
{
	return stagnationLimit;
}

void BeeSettings::setNeighbourhoodSize(float diameter)
{
	neighbourhoodSize=diameter;
}

float BeeSettings::getNeighbourhoodSize()
{
	return neighbourhoodSize;
}

void BeeSettings::setBeeSettingsFile(char* fileName)
{
	beeSettingsFile=new char[strlen(fileName)+1];

	strcpy(beeSettingsFile, fileName);
}

char* BeeSettings::getBeeSettingsFile()
{
	return beeSettingsFile;
}
