// bees.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "beesalgorithm.h"
#include "beesettings.h"
#include "populationsorter.h"
#include "problemsettings.h"
#include "settings.h"
#include "simulationsettings.h"
#include "time.h"
using namespace std;

void printHeaders(int variables, ofstream &outFile);

int main()
{
	int i;
	int iteration;
	time_t finish;
	time_t start;
	BeesAlgorithm* bayBA;
	PopulationSorter* sorter=NULL;
	Settings* allParameters;

	// parameters are read.
	allParameters=new Settings();
	allParameters->readSettings("settings/beesettings.par", "settings/simulationsettings.par", "settings/problemsettings.par");
	//cout << "parameters were read.\n";

	// main classes are initialised.
	bayBA=new BeesAlgorithm();
	bayBA->setMaker(new SwarmMaker());
	bayBA->setParameters(allParameters);
	bayBA->setSorter(new PopulationSorter());
	//cout << "main classes were initialised.\n";

	// results file is created.
	ofstream outFile(allParameters->getSimulationParameters()->getResultsFile());
	printHeaders(allParameters->getTestProblemSettings()->getInputs(), outFile);
	//cout << "results file was created.\n";

	// algorithm is run.
	cout << "algorithm running.\n";
	iteration=allParameters->getSimulationParameters()->getTrials();
	for(i=0;i<iteration;i++)
	{
		//cout << "iteration " << i << "\n";
		time(&start);
		bayBA->learn(i, outFile);
		time(&finish);
		outFile 
			<< difftime(finish, start)
			<< "\n";
	}
	//// end of tests.
	//cout << "end of tests\n";
	outFile.close();
	ofstream endFile("the_end.txt");
	return 0;
}

void printHeaders(int variables, ofstream &outFile)
{
	int i;
	char var[10];

	for(i=0;i<variables;i++)
	{
		ostringstream convert;
		convert << i; 
		strcpy (var,"x");
		strncat(var,convert.str().c_str(),2);
		outFile 
			<< var
			<< " "; 
	}
	outFile 
		<< "desired " 
		<< "found " 
		<< "iterations "
		<< "evaluations "
		<< "secs"
		<< endl;
}