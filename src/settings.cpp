#include "stdafx.h"
#include "settings.h"

Settings::Settings(void)
{
	beeParameters=NULL;
	simulationParameters=NULL;
	testProblemSettings=NULL;
}

Settings::Settings(int n,	int e, int m,	int ne,	int nb, int cycles, float ngh) : simulationParameters(NULL), testProblemSettings(new ProblemSettings())
{
	beeParameters=new BeeSettings(n,e,m,ne,nb,cycles,ngh);
}

Settings::~Settings(void)
{
	if(beeParameters!=NULL)
	{
		delete beeParameters;
	}
	if(simulationParameters!=NULL)
	{
		delete simulationParameters;
	}
	if(testProblemSettings!=NULL)
	{
		delete testProblemSettings;
	}
}

/* ---------------------------------------------------- */
/* this function manages the creation and				*/
/* initialisation of the three classes containing the	*/
/* parameters that define the experiment.				*/
/* input: none.										*/
/* return: none.										*/
/* ---------------------------------------------------- */
void Settings::readSettings(char* beeSettingsFileName, char* testSettingsFileName, char* problemSettingsFileName)
{
	beeParameters=new BeeSettings(beeSettingsFileName);
	beeParameters->readSettings();
	simulationParameters=new SimulationSettings(testSettingsFileName);
	simulationParameters->readSettings();
	testProblemSettings=new ProblemSettings(problemSettingsFileName);
	testProblemSettings->readSettings();
}

void Settings::setBeeParameters(BeeSettings* parameters)
{
	beeParameters=parameters;
}

BeeSettings* Settings::getBeeParameters()
{
	return beeParameters;
}

void Settings::setSimulationParameters(SimulationSettings* parameters)
{
	simulationParameters=parameters;
}

SimulationSettings* Settings::getSimulationParameters()
{
	return simulationParameters;
}

void Settings::setTestProblemSettings(ProblemSettings* parameters)
{
	testProblemSettings=parameters;
}

ProblemSettings* Settings::getTestProblemSettings()
{
	return testProblemSettings;
}


