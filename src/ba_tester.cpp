
#include <beesalgorithm.h>
#include <cmath>

using namespace std;

float Ackley(float* point, unsigned int n_dims){
	float sum1, sum2;
	sum1=sum2=0;
	for(unsigned int i=0;i<n_dims;i++)	{
		sum1+=powf(point[i], 2);
		sum2+=cos(2*M_PI*point[i]);
	}
	sum1/=n_dims;
	sum2/=n_dims;
	return 20+exp(1.0F)-20*exp(-0.2F*sqrt(sum1))-exp(sum2);
}

void printHeaders(int variables, ofstream &outFile)
{
	int i;
	char var[10];

	for(i=0;i<variables;i++)
	{
		ostringstream convert;
		convert << i;
		strcpy (var,"x");
		strncat(var,convert.str().c_str(),2);
		outFile
			<< var
			<< " ";
	}
	outFile
		<< "desired "
		<< "found "
		<< "iterations "
		<< "evaluations "
		<< "secs"
		<< endl;
}

int main(){

	unsigned int n, e, m, ne, nb, cycles, n_dimensions;
	float ngh, range;
	n=100;			/* size n of colony (n=ne·nre+(nb-ne)·nrb+ns) */
	e=1;			/* elite sites (ne, ne<=nb) */
	m=8;			/* best sites (nb) */
	ne=20;			/* recruited nre bees for elite sites */
	nb=10;			/* recruited nrb bees for best sites */
	ngh=1.0F;			/* initial size a(0) of neighbourhood. This is expressed as a fraction of a variable's range */
	cycles=5;			/* limit stlim of stagnation cycles for site abandonment */
	n_dimensions=10;
	range=32;
	BeesAlgorithm* bayBA=new BeesAlgorithm(Ackley,n_dimensions,range,n, e, m, ne, nb, cycles, ngh);
	bayBA->setStoppingCriterion(fixedCycles,5000);
	bayBA->setStoppingCriterion(fitness,0.001F);

	ofstream outFile("test_out.txt");
	printHeaders(n_dimensions, outFile);

	cout << "algorithm running.\n";
	unsigned int n_iterations=50;
	time_t start;
	time_t finish;
	for(unsigned int i=0;i<n_iterations;i++)
	{
		time(&start);
		bayBA->learn(i, outFile);
		time(&finish);
		outFile
			<< difftime(finish, start)
			<< "\n";
	}
	outFile.close();
	delete bayBA;
}
