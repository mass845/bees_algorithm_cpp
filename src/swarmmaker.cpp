#include "stdafx.h"
#include "swarmmaker.h"

SwarmMaker::SwarmMaker(void) : useCustomFunction(false)
{
}

SwarmMaker::SwarmMaker(unsigned int n_dim,float ran) : useCustomFunction(true), n_dimensions(n_dim), custom_range(ran)
{
}

SwarmMaker::~SwarmMaker(void)
{
}

/* ---------------------------------------------------- */
/* this function creates a swarm of randomly generated  */
/* solutions (i.e. bees).								*/
/* input: desired size of swarm.						*/
/* input: test settings.								*/
/* return: the swarm, (array of pointers to solutions). */
/* ---------------------------------------------------- */
Solution** SwarmMaker::createRandomSwarm(int swarmSize, Settings* parameters)
{
	int i;
	float range;
	float rate;
	Solution** swarm=new Solution*[swarmSize];
	range=parameters->getBeeParameters()->getNeighbourhoodSize();
	rate=0;
	for(i=0;i<swarmSize;i++){
		swarm[i]=createSolution(parameters);
		swarm[i]->setParametersMutationRange(range);
	}
	return swarm;
}

/* ---------------------------------------------------- */
/* this function creates a randomly generated solution  */
/* (i.e. bee).											*/
/* input: test settings.								*/
/* return: the solution.								*/
/* ---------------------------------------------------- */
Solution* SwarmMaker::createSolution(Settings* parameters)
{
	int i;
	int nrOfInputs;
	float* initialParameters;
	Solution* newSolution;
	ObjectiveFunction* function=new ObjectiveFunction();
	ProblemSettings* solutionParameters=parameters->getTestProblemSettings();
	if(useCustomFunction){
		nrOfInputs=n_dimensions;
	}else{
		nrOfInputs=solutionParameters->getInputs();
		function->setFunction(solutionParameters->getFunction());
	}
	function->setInputs(nrOfInputs);

	newSolution=new Solution();

	newSolution->setFunction(function);
	newSolution->initialiseParameters(nrOfInputs);
	initialParameters=newSolution->getFunction()->getInputPattern();
	for(i=0;i<nrOfInputs;i++)
	{
		newSolution->setParameter(i, initialParameters[i]);
	}

	delete[] initialParameters;
	return newSolution;
}

/* ---------------------------------------------------- */
/* this function randomly modifies the parameters of a  */
/* solution. The parameters are mutated of a delta		*/
/* randomly sampled with uniform probability within a	*/
/* pre-defined interval. 								*/
/* input: the solution undergoing mutation.				*/
/* return: none.										*/
/* ---------------------------------------------------- */
void SwarmMaker::mutateParameters(Solution* mutant)
{
	int i;
	int inputs;
	float functionRange;
	if(useCustomFunction){
		inputs=n_dimensions;
		functionRange=custom_range;
	}else{
		inputs=mutant->getFunction()->getInputs();
		functionRange=mutant->getFunction()->getVariablesRange();
	}
	float mutationRange=mutant->getParametersMutationRange()*functionRange; // half-width of mutation interval
	float incr;

	for(i=0;i<inputs;i++)
	{
		incr=2.0F*mutationRange*(rand()/(RAND_MAX+0.0F))-(mutationRange);
		mutant->incrementParameter(i, incr);
	}
}
