#include "stdafx.h"
#include "simulationsettings.h"
using namespace std;

SimulationSettings::SimulationSettings(char* settingsFileName)
{
	setSimulationSettingsFile(settingsFileName);
	resultsFile=NULL;
}

SimulationSettings::~SimulationSettings(void)
{
	if(resultsFile!=NULL)
	{
		delete[] resultsFile;
	}
	delete[] simulationSettingsFile;
}

/* ---------------------------------------------------- */
/* this function reads the parameters defining the		*/ 
/* experimental test.									*/
/* input: none.										*/
/* return: none.										*/
/* ---------------------------------------------------- */
void SimulationSettings::readSettings()
{
	char comment[200];
	int repetitions;
	int cycles;
	char fileName[200];	
	ifstream inFile; 

	inFile.open(getSimulationSettingsFile());
	if(!inFile)
	{
		cerr<<"can' t open Simulation settings file!\n";
		exit(1);
	}

	inFile >> repetitions;
	inFile.getline(comment,200);
	inFile >> cycles;
	inFile.getline(comment,200);
	inFile >> fileName;
	inFile.getline(comment,200);

	inFile.close();

	setTrials(repetitions);
	setIterations(cycles);
	setResultsFile(fileName);
}

void SimulationSettings::setTrials(int nr)
{
	trials=nr;
}

int SimulationSettings::getTrials()
{
	return trials;
}

void SimulationSettings::setIterations(int nr)
{
	iterations=nr;
}

int SimulationSettings::getIterations()
{
	return iterations;
}

void SimulationSettings::setResultsFile(char* fileName)
{
	resultsFile=new char[strlen(fileName)+1];

	strcpy(resultsFile, fileName);
}

char* SimulationSettings::getResultsFile()
{
	return resultsFile;
}

void SimulationSettings::setSimulationSettingsFile(char* fileName)
{
	simulationSettingsFile=new char[strlen(fileName)+1];

	strcpy(simulationSettingsFile, fileName);
}

char* SimulationSettings::getSimulationSettingsFile()
{
	return simulationSettingsFile;
}
