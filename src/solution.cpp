#include "stdafx.h"
#include "solution.h"

Solution::Solution(void)
{
	parameters=NULL;
	function=NULL;
}

Solution::~Solution(void)
{
	if(parameters!=NULL)
	{
		delete[] parameters;
	}
	if(function!=NULL)
	{
		delete function;
	}
}

/* ---------------------------------------------------- */
/* this function duplicates an instance of the class.	*/
/* input: none.										*/
/* return: the duplicated instance.						*/
/* ---------------------------------------------------- */
Solution* Solution::duplicate()
{
	int i;
	int nrOfInputs=getFunction()->getInputs();
	Solution* copy;

	copy=new Solution();

	copy->setFunction(getFunction()->duplicate());
	copy->initialiseParameters(nrOfInputs);
	for(i=0;i<nrOfInputs;i++)
	{
		copy->setParameter(i, getParameter(i));
	}
	copy->setParametersMutationRange(getParametersMutationRange());
	copy->setFitness(getFitness());

	return copy;
}

void Solution::setFitness(float value)
{
	fitness=value;
}

float Solution::getFitness()
{
	return fitness;
}

/* ---------------------------------------------------- */
/* this function stores the width of the mutation		*/
/* events for the solution. This parameter is expressed */
/* as a fraction of the	half-width of the variable's	*/
/* range. The function checks also that the value is in */
/* the interval [0.0001,1]. If it is outside, it gets	*/
/* rounded to the closest extreme.  					*/
/* input: mutation width.								*/
/* return: none.										*/
/* ---------------------------------------------------- */
void Solution::setParametersMutationRange(float value)
{
	if(value<0.00001F)
	{
		value=0.00001F;
	}
	if(value>1.0F)
	{
		value=1.0F;
	}

	parametersMutationRange=value;
}

float Solution::getParametersMutationRange()
{
	return parametersMutationRange;
}

/* ---------------------------------------------------- */
/* this function initialises the vector of parameters	*/
/* that characterises a solution.						*/
/* input: dimensionality of search space.				*/
/* return: none.										*/
/* ---------------------------------------------------- */
void Solution::initialiseParameters(int nrOfParameters)
{
	parameters=new float[nrOfParameters];
}

/* ---------------------------------------------------- */
/* this function instanciates a given parameter	of a	*/
/* solution. If the value is outside the variable's     */
/* range of definition, it is set to the closest		*/
/* extreme.												*/
/* input: parameter number.								*/
/* input: parameter value.								*/
/* return: none.										*/
/* ---------------------------------------------------- */
void Solution::setParameter(int nr, float value)
{
	if(value<-getRange())
	{
		value=-getRange();
	}
	if(value>getRange())
	{
		value=getRange();
	}
	parameters[nr]=value;
}

float Solution::getParameter(int nr)
{
	return parameters[nr];
}

/* ---------------------------------------------------- */
/* this function increments the value of a given		*/
/* paramete of an amount equal to delta. If the new		*/
/* parameter value is outside the variable's range of	*/
/* definition, it is set to the closest extreme			*/
/* input: parameter number.								*/
/* input: increment.								*/
/* return: none.										*/
/* ---------------------------------------------------- */
void Solution::incrementParameter(int nr, float delta)
{
	parameters[nr]+=delta;
	if(parameters[nr]<-getRange())
	{
		parameters[nr]=-getRange();
	}
	if(parameters[nr]>getRange())
	{
		parameters[nr]=getRange();
	}
}

void Solution::setFunction(ObjectiveFunction* func)
{
	function=func;
	range=func->getVariablesRange();
}

ObjectiveFunction* Solution::getFunction()
{
	return function;
}

float Solution::getFunctionOutput()
{
	return getFunction()->getOutput(parameters);
}

void Solution::setRange(float span)
{
	range=span;
}
float Solution::getRange()
{
	return range;
}

