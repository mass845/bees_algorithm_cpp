#include "stdafx.h"
#include "objectivefunction.h"
using namespace std;

ObjectiveFunction::ObjectiveFunction(void)
{
}

ObjectiveFunction::~ObjectiveFunction(void)
{
}

/* ---------------------------------------------------- */
/* this function duplicates an instance of the class.	*/
/* input: none.											*/
/* return: the duplicated instance.						*/
/* ---------------------------------------------------- */
ObjectiveFunction* ObjectiveFunction::duplicate()
{
	ObjectiveFunction* copy;

	copy=new ObjectiveFunction();

	copy->setInputs(getInputs());
	copy->setFunction(getFunction());

	return copy;
}

void ObjectiveFunction::setFunction(benchmarkFunction benchmark)
{
	function=benchmark;
}

benchmarkFunction ObjectiveFunction::getFunction()
{
	return function;
}

void ObjectiveFunction::setInputs(int nr)
{
	inputs=nr;
}

int ObjectiveFunction::getInputs()
{
	return inputs;
}

/* ---------------------------------------------------- */
/* this function creates a random vector of parameters  */
/* within the boundaries of the benchmark function.		*/
/* input: none.											*/
/* return: the random vector of parameters.				*/
/* ---------------------------------------------------- */
float* ObjectiveFunction::getInputPattern()
{
	int i;
	int vars=getInputs();
	float random;
	float initRange=getVariablesRange();
	float* pattern=new float[vars];

	for(i=0;i<vars;i++)
	{
		random=rand()/(RAND_MAX+0.0F);
		pattern[i]=2*random*initRange-initRange;
	}
	return pattern;
}

/* ---------------------------------------------------- */
/* this function encodes the mapping of the benchmark	*/
/* function. It returns the value taken by the			*/ 
/* function on a given point of the search space.		*/
/* input: the position on the search space.				*/
/* return: the output of the function.					*/
/* ---------------------------------------------------- */
float ObjectiveFunction::getOutput(float* inputPattern)
{
	benchmarkFunction benchmark=getFunction();
	int i;
	int j;
	int vars=getInputs();
	float sum1;
	float sum2;
	float sine;
	float prod;
	float squaredTerm;
	float pi=acosf(-1);
	float e=exp(1.0F);
	float output;

	if(benchmark==ackley)
	{
		sum1=0;
		sum2=0;
		for(i=0;i<vars;i++)
		{
			sum1+=powf(inputPattern[i], 2);
			sum2+=cos(2*pi*inputPattern[i]);
		}
		sum1/=vars;
		sum2/=vars;
		output=20+e-20*exp(-0.2F*sqrt(sum1))-exp(sum2);
	}
	else if(benchmark==easom)
	{
		float x1=inputPattern[0];
		float x2=inputPattern[1];
		output=-cos(x1)*cos(x2)*exp(-powf(x1-pi, 2)-powf(x2-pi, 2));
	}
	else if(benchmark==goldstein_price)
	{
		float x1=inputPattern[0];
		float x2=inputPattern[1];
		float term1;
		float term2;
		term1=1+powf((x1+x2+1),2)*(19-14*x1+3*x1*x1-14*x2+6*x1*x2+3*x2*x2);
		term2=30+powf((2*x1-3*x2),2)*(18-32*x1+12*x1*x1+48*x2-36*x1*x2+27*x2*x2);
		output=term1*term2;
	}
	else if(benchmark==griewank)
	{
		sum1=0;
		for(i=0;i<vars;i++)
		{
			sum1+=powf(inputPattern[i]-100, 2);
		}
		sum1/=4000;
		prod=1;
		for(i=0;i<vars;i++)
		{
			prod*=cos((inputPattern[i]-100)/sqrt(i+1.0F));
		}
		output=sum1-prod+1;
	}
	else if(benchmark==hypersphere)
	{
		output=0;
		for(i=0;i<vars;i++)
		{
			output+=powf(inputPattern[i], 2);
		}
	}
	else if(benchmark==langermann)
	{
		float a;
		float c;
		output=0.0F;
		for(j=0;j<5;j++)
		{
			sum1=0.0F;
			for(i=0;i<vars;i++)
			{
				a=shekelMatrix(j, i);
				sum1+=powf((inputPattern[i]+5)-a, 2);
			}
			c=langermannVector(j);
			output+=c*expf(-(sum1/pi))*cosf(pi*sum1);
		}
	}
	else if(benchmark==martin_gaddy)
	{
		float x1=inputPattern[0];
		float x2=inputPattern[1];
		output=powf(x1-x2, 2)+powf((x1+x2-10)/3.0F, 2);
	}
	else if(benchmark==michalewicz)
	{
		output=0;
		float term1;
		float term2;
		float x;
		for(i=0;i<vars;i++)
		{
			x=inputPattern[i]+pi/2.0F;
			term1=sin(x);
			term2=sin(((1+i)*x*x)/pi);
			output-=term1*powf(term2, 20);
		}
	}
	else if(benchmark==rastrigin)
	{
		output=0;
		for(i=0;i<vars;i++)
		{
			output+=powf(inputPattern[i], 2)-10*cos(2*pi*inputPattern[i])+10;
		}
	}
	else if(benchmark==rosenbrock)
	{
		output=0;
		for(i=0;i<vars-1;i++)
		{
			squaredTerm=powf(inputPattern[i], 2);
			output+=100*powf(inputPattern[i+1]-squaredTerm, 2)+powf(inputPattern[i]-1, 2);
		}
	}
	else if(benchmark==schaffer)
	{
		sum1=powf(inputPattern[0], 2)+powf(inputPattern[1], 2);
		sine=sin(sqrt(sum1));
		output=0.5F+(powf(sine, 2)-0.5F)/powf((1+0.001F*sum1), 2);
	}
	else if(benchmark==schwefel)
	{
		sum1=0;
		for(i=0;i<vars;i++)
		{
			sum1+=inputPattern[i]*sin(sqrt(fabsf(inputPattern[i])));
		}
		output=-sum1;
	}
	else if(benchmark==shekel)
	{
		float a;
		float c;
		output=0.0F;
		for(j=0;j<30;j++)
		{
			sum1=0.0F;
			for(i=0;i<vars;i++)
			{
				a=shekelMatrix(j, i);
				sum1+=powf((inputPattern[i]+5)-a, 2);
			}
			c=shekelVector(j);
			output-=1/(c+sum1);
		}
	}
	else if(benchmark==steps)
	{
		output=0.0F;
		for(i=0;i<vars;i++)
		{
			output+=int(inputPattern[i]);
		}
	}

	return output;
}

/* ---------------------------------------------------- */
/* this function gives the global minimum of the 		*/
/* benchmark function.									*/
/* input: none.											*/
/* return: the global minimum.							*/
/* ---------------------------------------------------- */
float ObjectiveFunction::getOptimum()
{
	benchmarkFunction benchmark=getFunction();
	float optimum;

	if(benchmark==ackley)
	{
		optimum=0;						// f(0)=0
	}
	else if(benchmark==easom)
	{
		optimum=-1;						// f(pi,pi)=-1
	}
	else if(benchmark==goldstein_price)
	{
		optimum=3;						// f(0,-1)=3
	}
	else if(benchmark==griewank)
	{
		optimum=0;						// f(100)=0
	}
	else if(benchmark==hypersphere)
	{
		optimum=0;						// f(0)=0
	}
	else if(benchmark==langermann)
	{
		optimum=-0.705525F;					// f()=-1.4
	}
	else if(benchmark==martin_gaddy)
	{
		optimum=0;						// f(5,5)=0
	}
	else if(benchmark==michalewicz)
	{
		optimum=-9.66F;					// f()=-9.66
	}
	else if(benchmark==rastrigin)
	{
		optimum=0;						// f(0)=0
	}
	else if(benchmark==rosenbrock)
	{
		optimum=0;						// f(1)=0
	}
	else if(benchmark==schaffer)
	{
		optimum=0;						// f(0,0)=0
	}
	else if(benchmark==schwefel)
	{
		optimum=-getInputs()*418.9829F;			// f(420.9687,420.9687)=-837.9658
	}
	else if(benchmark==shekel)
	{
		optimum=-10.2021F;				// f(3.02,4.15,0.11,2.62,-0.44,-0.29,-2.00,1.13,-4.27,-0.018)=-10.2021
	}
	else if(benchmark==steps)
	{
		optimum=-5.0F*getInputs();				
	}

	return optimum;
}

/* ---------------------------------------------------- */
/* this function gives the half-width W of the interval */
/* of definition of the benchmark function.	The			*/
/* function	is therefore defined in the interval		*/
/* [-W,+W].												*/
/* input: none.											*/
/* return: the half width.								*/
/* ---------------------------------------------------- */
float ObjectiveFunction::getVariablesRange()
{
	benchmarkFunction benchmark=getFunction();
	float pi=2*asinf(1.0F);
	float range;

	if(benchmark==ackley)
	{
		range=32;
	}
	else if(benchmark==easom)
	{
		range=100;
	}
	else if(benchmark==goldstein_price)
	{
		range=2;
	}
	else if(benchmark==griewank)
	{
		range=600;
	}
	else if(benchmark==hypersphere)
	{
		range=100;
	}
	else if(benchmark==langermann)
	{
		range=5;
	}
	else if(benchmark==martin_gaddy)
	{
		range=20;
	}
	else if(benchmark==michalewicz)
	{
		range=pi/2.0F;
	}
	else if(benchmark==rastrigin)
	{
		range=5.12F;
	}
	else if(benchmark==rosenbrock)
	{
		range=50;
	}
	else if(benchmark==schaffer)
	{
		range=100;
	}
	else if(benchmark==schwefel)
	{
		range=500;
	}
	else if(benchmark==shekel)
	{
		range=5;
	}
	else if(benchmark==steps)
	{
		range=5.12F;
	}

	return range;
}

/* ---------------------------------------------------- */
/* this function returns the elements of the matrix of  */
/* parameters that characterises the Shekel function.	*/
/* input: matrix entry.									*/
/* return: parameter.									*/
/* ---------------------------------------------------- */
float ObjectiveFunction::shekelMatrix(int i, int j)
{
	float A[30][10]={
		{9.681F, 0.667F, 4.783F, 9.095F, 3.517F, 9.325F, 6.544F, 0.211F, 5.122F, 2.020F},
		{9.400F, 2.041F, 3.788F, 7.931F, 2.882F, 2.672F, 3.568F, 1.284F, 7.033F, 7.374F},
		{8.025F, 9.152F, 5.114F, 7.621F, 4.564F, 4.711F, 2.996F, 6.126F, 0.734F, 4.982F},
		{2.196F, 0.415F, 5.649F, 6.979F, 9.510F, 9.166F, 6.304F, 6.054F, 9.377F, 1.426F},
		{8.074F, 8.777F, 3.467F, 1.863F, 6.708F, 6.349F, 4.534F, 0.276F, 7.633F, 1.567F},
		{7.650F, 5.658F, 0.720F, 2.764F, 3.278F, 5.283F, 7.474F, 6.274F, 1.409F, 8.208F},
		{1.256F, 3.605F, 8.623F, 6.905F, 0.584F, 8.133F, 6.071F, 6.888F, 4.187F, 5.448F},
		{8.314F, 2.261F, 4.224F, 1.781F, 4.124F, 0.932F, 8.129F, 8.658F, 1.208F, 5.762F},
		{0.226F, 8.858F, 1.420F, 0.945F, 1.622F, 4.698F, 6.228F, 9.096F, 0.972F, 7.637F},
		{305.0F, 2.228F, 1.242F, 5.928F, 9.133F, 1.826F, 4.060F, 5.204F, 8.713F, 8.247F},
		{0.652F, 7.027F, 0.508F, 4.876F, 8.807F, 4.632F, 5.808F, 6.937F, 3.291F, 7.016F},
		{2.699F, 3.516F, 5.874F, 4.119F, 4.461F, 7.496F, 8.817F, 0.690F, 6.593F, 9.789F},
		{8.327F, 3.897F, 2.017F, 9.570F, 9.825F, 1.150F, 1.395F, 3.885F, 6.354F, 0.109F},
		{2.132F, 7.006F, 7.136F, 2.641F, 1.882F, 5.943F, 7.273F, 7.691F, 2.880F, 0.564F},
		{4.707F, 5.579F, 4.080F, 0.581F, 9.698F, 8.542F, 8.077F, 8.515F, 9.231F, 4.670F},
		{8.304F, 7.559F, 8.567F, 0.322F, 7.128F, 8.392F, 1.472F, 8.524F, 2.277F, 7.826F},
		{8.632F, 4.409F, 4.832F, 5.768F, 7.050F, 6.715F, 1.711F, 4.323F, 4.405F, 4.591F},
		{4.887F, 9.112F, 0.170F, 8.967F, 9.693F, 9.867F, 7.508F, 7.770F, 8.382F, 6.740F},
		{2.440F, 6.686F, 4.299F, 1.007F, 7.008F, 1.427F, 9.398F, 8.480F, 9.950F, 1.675F},
		{6.306F, 8.583F, 6.084F, 1.138F, 4.350F, 3.134F, 7.853F, 6.061F, 7.457F, 2.258F},
		{0.652F, 2.343F, 1.370F, 0.821F, 1.310F, 1.063F, 0.689F, 8.819F, 8.833F, 9.070F},
		{5.558F, 1.272F, 5.756F, 9.857F, 2.279F, 2.764F, 1.284F, 1.677F, 1.244F, 1.234F},
		{3.352F, 7.549F, 9.817F, 9.437F, 8.687F, 4.167F, 2.570F, 6.540F, 0.228F, 0.027F},
		{8.798F, 0.880F, 2.370F, 0.168F, 1.701F, 3.680F, 1.231F, 2.390F, 2.499F, 0.064F},
		{1.460F, 8.057F, 1.336F, 7.217F, 7.914F, 3.615F, 9.981F, 9.198F, 5.292F, 1.224F},
		{0.432F, 8.645F, 8.774F, 0.249F, 8.081F, 7.461F, 4.416F, 0.652F, 4.002F, 4.644F},
		{0.679F, 2.800F, 5.523F, 3.049F, 2.968F, 7.225F, 6.730F, 4.199F, 9.614F, 9.229F},
		{4.263F, 1.074F, 7.286F, 5.599F, 8.291F, 5.200F, 9.214F, 8.272F, 4.398F, 4.506F},
		{9.496F, 4.830F, 3.150F, 8.270F, 5.079F, 1.231F, 5.731F, 9.494F, 1.883F, 9.732F},
		{4.138F, 2.562F, 2.532F, 9.661F, 5.611F, 5.500F, 6.886F, 2.341F, 9.699F, 6.500F},
	};

	return A[i][j];
}

/* ---------------------------------------------------- */
/* this function returns the components of the vector   */
/* of parameters that characterises the Shekel			*/
/* function.											*/
/* input: vector element.								*/
/* return: parameter.									*/
/* ---------------------------------------------------- */
float ObjectiveFunction::shekelVector(int i)
{
	float c[30]={
		0.806F, 0.517F, 0.100F, 0.908F, 0.965F, 0.669F, 0.524F, 0.902F, 0.531F, 0.876F, 
		0.462F, 0.491F, 0.463F, 0.714F, 0.352F, 0.869F, 0.813F, 0.811F, 0.828F, 0.964F, 
		0.789F, 0.360F, 0.369F, 0.992F, 0.332F, 0.817F, 0.632F, 0.883F, 0.608F, 0.326F};

		return c[i];
}

/* ---------------------------------------------------- */
/* this function returns the parameters that define		*/
/* the peaks of the Langermann function.				*/
/* input: parameter number.								*/
/* return: parameter.									*/
/* ---------------------------------------------------- */
float ObjectiveFunction::langermannVector(int i)
{
	float c[30]={
		0.806F, 0.517F, 0.150F, 0.908F, 0.965F, 0.669F, 0.524F, 0.902F, 0.531F, 0.876F, 
		0.462F, 0.491F, 0.463F, 0.714F, 0.352F, 0.869F, 0.813F, 0.811F, 0.828F, 0.964F, 
		0.789F, 0.360F, 0.369F, 0.992F, 0.332F, 0.817F, 0.632F, 0.883F, 0.608F, 0.326F};

		return c[i];
}