#include "stdafx.h"
#include "beesalgorithm.h"
#include "swarmmaker.h"

BeesAlgorithm::BeesAlgorithm(void) : custom_objective_function(NULL), n_dimensions(0)
{
}
BeesAlgorithm::BeesAlgorithm(float (*f)(float*,unsigned int), unsigned int n_dim, float ran, int n,	int e, int m,	int ne,	int nb, int cycles, float ngh)
									: custom_objective_function(f), n_dimensions(n_dim), range(ran)
{
	setSorter(new PopulationSorter());
	setMaker(new SwarmMaker(n_dimensions,range));
	setParameters(new Settings(n,e,m,ne,nb,cycles,ngh));
}
BeesAlgorithm::~BeesAlgorithm(void)
{
}

/* ---------------------------------------------------- */
/* this function initialises the algorithm.				*/
/* input: none.											*/
/* return: none.										*/
/* ---------------------------------------------------- */
void BeesAlgorithm::initialisation()
{
	int i;
	int nrOfScouts=getParameters()->getBeeParameters()->getScoutBees();
	// the initial population of scouts is created
	setSwarm(getMaker()->createRandomSwarm(nrOfScouts, getParameters()));
	// a flower patch is associated to each scout
	patches=new FlowerPatch*[nrOfScouts];
	for(i=0;i<nrOfScouts;i++)
	{
		patches[i]=new FlowerPatch();
		patches[i]->setImprovement(true);
		patches[i]->setScout(swarm[i]);
	}
	// the initial population is evaluated and sorted
	evaluation(getSwarm(), nrOfScouts);
	getSorter()->sort(nrOfScouts, getAllPatches());
	// the best-so-far solution is recorded
	setBestBee(getPatch(0)->getScout()->duplicate());
}

/* ---------------------------------------------------- */
/* this function deletes the population of scouts and   */
/* the associated flower patches.						*/
/* input: none.											*/
/* return: none.										*/
/* ---------------------------------------------------- */
void BeesAlgorithm::deletePopulation()
{
	int i;
	int nrOfScouts=getParameters()->getBeeParameters()->getScoutBees();

	for(i=0;i<nrOfScouts;i++)
	{
		delete getPatch(i);
		setPatch(i, NULL);
	}
	delete[] swarm;
	delete[] patches;
}

/* ---------------------------------------------------- */
/* this function evaluates the population fitness.		*/
/* input: population.									*/
/* input: population size.								*/
/* return: none.										*/
/* ---------------------------------------------------- */
void BeesAlgorithm::evaluation(Solution** population, int popSize)
{
	int p;

	for(p=0;p<popSize;p++)
	{
		checkAndSetFitness(population[p]);
//		population[p]->setFitness(population[p]->getFunctionOutput());
	}
}
/*
* Auxiliary function made by Luca Baronti
*/
void BeesAlgorithm::checkAndSetFitness(Solution* sol){
	if (custom_objective_function==NULL){
		sol->setFitness(sol->getFunctionOutput());
	}else{
		sol->setFitness((*custom_objective_function)(sol->getParameters(),n_dimensions));
	}
}

/* ---------------------------------------------------- */
/* this function implements the local search procedure.	*/
/* within a flower patch. it is used for both elite and */
/* best sites, the only difference being the number of  */
/* foragers involved.									*/
/* input: flower patch.									*/
/* return: none.										*/
/* ---------------------------------------------------- */
void BeesAlgorithm::harvest(FlowerPatch* foodSource)
{
	int i;
	int swarmSize=foodSource->getSwarmSize();
	Solution* newBee;
	Solution* fittestRecruited; // fittest forager
	Solution* scoutBee=(Solution*) foodSource->getScout(); // scout associated to flower patch

	foodSource->setImprovement(false); // set improvement on this patch to false
	// create and evaluate first forager
	newBee=scoutBee->duplicate();
	getMaker()->mutateParameters(newBee); // uniform mutation
	checkAndSetFitness(newBee);
	checkAndSetFitness(scoutBee);
//	newBee->setFitness(newBee->getFunctionOutput());
//	scoutBee->setFitness(scoutBee->getFunctionOutput());
	if(newBee->getFitness()<scoutBee->getFitness()) // keep the fittest bee between the scout and the forager
	{
		foodSource->setImprovement(true);
		fittestRecruited=newBee;	// the forager is the fittest bee
	}
	else
	{
		fittestRecruited=scoutBee->duplicate();
		delete newBee;	// the scout is the fittest bee, a copy of the scout is kept
	}

	for(i=0;i<swarmSize-1;i++) // repeat for all other foragers the procedure above
	{
		newBee=fittestRecruited->duplicate();
		getMaker()->mutateParameters(newBee); // uniform mutation
		checkAndSetFitness(newBee);
		checkAndSetFitness(fittestRecruited);
//		newBee->setFitness(newBee->getFunctionOutput());
//		fittestRecruited->setFitness(fittestRecruited->getFunctionOutput());
		if(newBee->getFitness()<fittestRecruited->getFitness())
		{
			foodSource->setImprovement(true);
			delete fittestRecruited;
			fittestRecruited=newBee;  // keep the new solution if it is the fittest-so-far solution generated within the flower patch
		}
		else
		{
			delete newBee; // otherwise delete the new bee, the fittest bee is unchanged
		}
	}

	foodSource->setScout(fittestRecruited); // set the new scout of the flower patch as the fittest solution generated by the local search procedure
}

/* ---------------------------------------------------- */
/* this function manages the local search procedure.	*/
/* input: number of flower patches (elite+best).		*/
/* return: none.										*/
/* ---------------------------------------------------- */
void BeesAlgorithm::localSearch(int selectedSites)
{
	int i;
	FlowerPatch* foodSource;

	for(i=0;i<selectedSites;i++)
	{
		foodSource=getPatch(i);
		harvest(foodSource);
	}
}

/* ---------------------------------------------------- */
/* this function generates a randomly initialised		*/
/* flower patch.										*/
/* input: none.											*/
/* return: the flower patch.							*/
/* ---------------------------------------------------- */
FlowerPatch* BeesAlgorithm::randomPatch()
{
	float neighbourhood=getParameters()->getBeeParameters()->getNeighbourhoodSize(); // neighbourhood size for local search
	FlowerPatch* newPatch=new FlowerPatch();
	Solution* scoutBee;
	SwarmMaker* motherOfBees=getMaker();

	Solution* newBee=motherOfBees->createSolution(parameters);
	newBee->setParametersMutationRange(neighbourhood);
	scoutBee=newBee;

	newPatch->setImprovement(true);
	newPatch->setScout(scoutBee);
	evaluation(&scoutBee, 1); // the fitness of the scout bee is evaluated

	return newPatch;
}

/* ---------------------------------------------------- */
/* this function implements the global search			*/
/* procedure. it generates a set of random scout bees.	*/
/* input: number of random scout bees.					*/
/* return: swarm of (array of pointers to) scouts.		*/
/* ---------------------------------------------------- */
FlowerPatch** BeesAlgorithm::globalSearch(int randomScouts)
{
	int i;
	FlowerPatch** newPatches;
	Solution** explorers;
	SwarmMaker* maker=getMaker();

	if(randomScouts>0)
	{
		explorers=new Solution*[randomScouts];
		newPatches=new FlowerPatch*[randomScouts];
		for(i=0;i<randomScouts;i++)
		{
			newPatches[i]=randomPatch();
			explorers[i]=(Solution*) newPatches[i]->getScout();
		}

		evaluation(explorers, randomScouts); // scouts are evaluated
		delete[] explorers;
	}
	else
	{
		newPatches=NULL;
	}
	return newPatches;
}

/* ---------------------------------------------------- */
/* this function implements the waggle dance (i.e.		*/
/* selection) procedure. Each best (elite) site is		*/
/* allocated a pre-defined number of scouts.			*/
/* input: none.											*/
/* return: none.										*/
/* ---------------------------------------------------- */
void BeesAlgorithm::waggleDance()
{
	int i;
	int scouts;
	int bestPatches;
	int elitePatches;
	int recruitedBest;
	int recruitedElite;
	BeeSettings* beeParameters=getParameters()->getBeeParameters();

	scouts=beeParameters->getScoutBees();
	bestPatches=beeParameters->getBestSites();
	elitePatches=beeParameters->getEliteSites();
	recruitedBest=beeParameters->getRecruitedBestSites();
	recruitedElite=beeParameters->getRecruitedEliteSites();
	// flower patches are sorted in decreasing order of fitness
	getSorter()->sort(scouts, getAllPatches());
	// 'e' elite sites are allocated 'ne' foragers
	for(i=0;i<elitePatches;i++)
	{
		getPatch(i)->setSwarmSize(recruitedElite);
	}
	// 'b-e' best sites are allocated 'nb'<='ne' foragers
	for(i=elitePatches;i<bestPatches;i++)
	{
		getPatch(i)->setSwarmSize(recruitedBest);
	}
	// remaining solutions are allocated no foragers
	for(i=bestPatches;i<scouts;i++)
	{
		getPatch(i)->setSwarmSize(0);
	}
}

/* ---------------------------------------------------- */
/* this function manages a whole iteration of the Bees	*/
/* Algorithm. It calls in order the relevant methods	*/
/* that implement the waggle dance, local and global	*/
/* search, and population update.						*/
/* input: none.											*/
/* return: none.										*/
/* ---------------------------------------------------- */
void BeesAlgorithm::foragingStep()
{
	int nrOfRandomScouts;
	BeeSettings* beeParameters=getParameters()->getBeeParameters();
	FlowerPatch** globalSearchScouts;
	SwarmMaker* motherOfBees=getMaker();

	nrOfRandomScouts=beeParameters->getScoutBees()-beeParameters->getBestSites();

	waggleDance();
	localSearch(beeParameters->getBestSites());
	globalSearchScouts=globalSearch(nrOfRandomScouts);
	updatePop(globalSearchScouts);
}

/* ---------------------------------------------------- */
/* this function implements the Bees Algorithm.			*/
/* It calls the relevant methods for initialisation of 	*/
/* the population, search, and monitoring				*/
/* input: iteration (several instances of an			*/
/* experiment are usually run for averaging purposes)	*/
/* input: file where the results are saved.				*/
/* return: none.										*/
/* ---------------------------------------------------- */
void BeesAlgorithm::learn(int trialNr, ofstream &outFile)
{
	bool stop=false;
	int gen;
	int scouts=getParameters()->getBeeParameters()->getScoutBees();
	Solution* bestSolution=NULL;
	// initialisation
	bestBee=NULL;
	initialisation();
	setRestarted(0);
	// main loop
	gen=0;
	while(!stop)
	{
		cout <<endl<< "iteration: " << trialNr << " generation : " << gen << "\n";
		foragingStep();
		gen++;
		// stoppong criterion is checked
		stop=stoppingCondition(getSwarm(), scouts, gen);
	}
	// final solution is saved on file
	printBest(gen, (Solution*) getBestBee(), outFile);

	// memory is freed
	//cout << "free population...\n";
	deletePopulation();
	delete getBestBee();
	//cout << "finished cycle...\n";
}
void BeesAlgorithm::setStoppingCriterion(terminatingCondition criterion, float value){
	parameters->getTestProblemSettings()->setStoppingCriterion(criterion);
	switch(criterion){
	case fixedCycles:
		max_iterations=value;
		break;
	case fitness:
		fitness_tollerance=value;
		break;
	}
}
/* ---------------------------------------------------- */
/* this method verifies whether the stopping criterion	*/
/* is met.												*/
/* input: scouts										*/
/* input: number of scouts.								*/
/* input: evolution cycle.								*/
/* return: result of test (true/false).					*/
/* ---------------------------------------------------- */
bool BeesAlgorithm::stoppingCondition(Solution** population, int popSize, int gen)
{
	terminatingCondition stopCondition;
	bool stop=false;
	int p;
	int iterations;
	float solutionFitness;
	float fitnessThreshold;
	float error;
	Solution* fittest;
	PopulationSorter* sortFitness=getSorter();
	// type of stopping condition is checked
	stopCondition=getParameters()->getTestProblemSettings()->getStoppingCriterion();
	if(stopCondition==fitness) // if stopping condition is fitness threshold
	{
		for(p=0;p<popSize;p++) // check how far each solution is from the global minimum of the benchmark function
		{
			if (custom_objective_function==NULL){
				solutionFitness=population[p]->getFunctionOutput();
				fitnessThreshold=getParameters()->getTestProblemSettings()->getAcceptableFitness(); // error tolerance
			}else{
				solutionFitness=(*custom_objective_function)(population[p]->getParameters(),n_dimensions);
				fitnessThreshold=fitness_tollerance; // error tolerance
			}

			error=fabsf(population[p]->getFunction()->getOptimum()-solutionFitness); // error = solution's fitness - global optimum of fitness
			population[p]->setFitness(error);
		}
		// sort the population in decreasing order of error (i.e. distance from minimum)
		sortFitness->sort(popSize, population);
		fittest=population[0]; // take fittest
		solutionFitness=fittest->getFitness();
		if(solutionFitness<fitnessThreshold) // if fittest is close enough to optimum, stop the search
		{
			stop=true;
		}
	}
	if (custom_objective_function==NULL){
		iterations=getParameters()->getSimulationParameters()->getIterations(); // otherwise check that maximum number of evolution cycles has not elapsed
	}else{
		iterations=max_iterations;
	}
	if(gen==iterations)
	{
		stop=true;
	}

	return stop;
}

/* ---------------------------------------------------- */
/* this method takes the current population and checks	*/
/* whether the fittest solution is also the best-so-far	*/
/* found. If so, the best-so-far solution is updated.	*/
/* input: none.											*/
/* return: none.										*/
/* ---------------------------------------------------- */
void BeesAlgorithm::updateBestBee()
{
	int scouts=getParameters()->getBeeParameters()->getScoutBees();
	float bestSoFar=getBestBee()->getFitness();
	float populationBest;

	getSorter()->sort(scouts, getAllPatches());
	populationBest=getPatch(0)->getScout()->getFitness();
	if(populationBest<bestSoFar)
	{
		setBestBee(getPatch(0)->getScout()->duplicate());
	}
}

/* ---------------------------------------------------- */
/* this method updates the population of scouts at the	*/
/* end of a learning cycle.								*/
/* input: array of pointers to random scouts generated 	*/
/* by global search	procedure.							*/
/* return: none.										*/
/* ---------------------------------------------------- */
void BeesAlgorithm::updatePop(FlowerPatch** globalSearchScouts)
{
	int i;
	int nrOfScouts;
	int bestBees;
	float neighbourhoodSize;
	BeeSettings* beeParameters=getParameters()->getBeeParameters();

	nrOfScouts=beeParameters->getScoutBees();
	bestBees=beeParameters->getBestSites();
	// update population according to results of local search
	for(i=0;i<bestBees;i++)
	{
		getPatch(i)->updateStagnation(); // update local search stagnation counter for each patch
		if(getPatch(i)->getStagnation()<beeParameters->getStagnationLimit())
		{
			if(getPatch(i)->getImprovement()==false) // if search in a flower patch has stagnated in this cycle
			{
				float temp=getPatch(i)->getWidth();
				neighbourhoodSize=0.8F*temp; // shrink neighbourhood
				getPatch(i)->setWidth(neighbourhoodSize);
			}
		}
		else // if stagnation limit has been reached in a flower patch
		{
			delete getPatch(i); // delete the patch
			setPatch(i, randomPatch()); // restart a new random one
			incrementRestarted(); // increment counter of restarted patches
		}
	}
	// delete old population
	for(i=0;i<nrOfScouts;i++)
	{
		delete getSolution(i);
	}
	// replace old population with results of local search
	for(i=0;i<bestBees;i++)
	{
		setSolution(i, getPatch(i)->getScout());
	}
	// fill in remaining spaces with results of global search
	for(i=bestBees;i<nrOfScouts;i++)
	{
		delete getPatch(i);
		setPatch(i, globalSearchScouts[i-bestBees]);
		setSolution(i, getPatch(i)->getScout());
	}
	updateBestBee(); // update best-so-far
	if(bestBees<nrOfScouts) // free memory used by global search scouts array of pointers (the solutions that it pointed to are already stored in the new population)
	{
		delete[] globalSearchScouts;
	}
}

/* ---------------------------------------------------- */
/* this method prints out the final result of the 		*/
/* search procedure.									*/
/* input: evolution cycle.								*/
/* input: final solution.								*/
/* input: output file.									*/
/* return: none.										*/
/* ---------------------------------------------------- */
void BeesAlgorithm::printBest(int iterations, Solution* best, ostream &outFile)
{
	int i;
	int variables=getParameters()->getTestProblemSettings()->getInputs();
	int scouts;
	int elite;
	int otherBest;
	int recruitedElite;
	int recruitedBest;
	int evaluationsPerIterations;
	BeeSettings* beeParameters=getParameters()->getBeeParameters();
	if (custom_objective_function==NULL){
		variables=getParameters()->getTestProblemSettings()->getInputs();
	}else{
		variables=n_dimensions;
	}
	elite=beeParameters->getEliteSites();
	otherBest=beeParameters->getBestSites()-elite;
	recruitedElite=beeParameters->getRecruitedEliteSites();
	recruitedBest=beeParameters->getRecruitedBestSites();
	scouts=beeParameters->getScoutBees()-beeParameters->getBestSites();
	evaluationsPerIterations=scouts+otherBest*recruitedBest+elite*recruitedElite; // fixed number of objective function evaluations per learning cycle

	for(i=0;i<variables;i++)	// print out optimised parameters (i.e. position of best in search space)
	{
		outFile
			<< best->getParameter(i)
			<< " ";
	}
	float solutionFitness;
	if (custom_objective_function==NULL){
		solutionFitness=best->getFunctionOutput();
	}else{
		solutionFitness=(*custom_objective_function)(best->getParameters(),n_dimensions);
	}
	outFile
		<< best->getFunction()->getOptimum() // desired result
		<< " "
		<< solutionFitness // result obtained
		<< " "
		<< iterations
		<< " "
		<< iterations*evaluationsPerIterations+getRestarted() // total number of objective function evaluations = fixed number of evaluations + evaluations of restarted flower patches (i.e. those that reached the stagnation limit)
		<< " ";
}

Solution* BeesAlgorithm::getSolution(int nr)
{
	return swarm[nr];
}

void BeesAlgorithm::setSolution(int nr, Solution* bee)
{
	swarm[nr]=bee;
}

void BeesAlgorithm::setSwarm(Solution** colony)
{
	swarm=colony;
}

Solution** BeesAlgorithm::getSwarm()
{
	return swarm;
}

void BeesAlgorithm::setPatch(int nr, FlowerPatch* patch)
{
	patches[nr]=patch;
}

FlowerPatch* BeesAlgorithm::getPatch(int nr)
{
	return patches[nr];
}

FlowerPatch** BeesAlgorithm::getAllPatches()
{
	return patches;
}

void BeesAlgorithm::setBestBee(Solution* bee)
{
	if(bestBee!=NULL)
	{
		delete bestBee;
	}
	bestBee=bee;
}

Solution* BeesAlgorithm::getBestBee()
{
	return bestBee;
}

void BeesAlgorithm::setMaker(SwarmMaker* creator)
{
	maker=creator;
}

SwarmMaker* BeesAlgorithm::getMaker()
{
	return maker;
}

void BeesAlgorithm::setParameters(Settings* settings)
{
	parameters=settings;
}

Settings* BeesAlgorithm::getParameters()
{
	return parameters;
}

void BeesAlgorithm::setSorter(PopulationSorter* routine)
{
	sorter=routine;
}

PopulationSorter* BeesAlgorithm::getSorter()
{
	return sorter;
}

void BeesAlgorithm::setRestarted(int count)
{
	restarted=count;
}

void BeesAlgorithm::incrementRestarted()
{
	restarted++;
}

int BeesAlgorithm::getRestarted()
{
	return restarted;
}
