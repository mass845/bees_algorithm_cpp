/* ---------------------------------------------------- */
/* this class stores the parameters defining the		*/
/* benchmark problem. These parameters are defined by   */
/* the user in file problemsettings.par.				*/
/* ---------------------------------------------------- */

#pragma once

class ProblemSettings
{
private:
	terminatingCondition stoppingCriterion;			/* terminating condition for algorithm (see definitions.h) */
	float acceptableFitness;						/* threshold of fitness used as stopping criterion */
	benchmarkFunction function;						/* function to be minimised (see definitions.h) */
	int inputs;										/* number of function inputs (i.e. parameters to optimise)*/
	char* problemSettingsFile;						/* file where data parameters are stored*/

public:
	ProblemSettings();
	ProblemSettings(char* settingsFileName);
	~ProblemSettings(void);
	void readSettings();
	void setStoppingCriterion(terminatingCondition criterion);
	terminatingCondition getStoppingCriterion();
	void setAcceptableFitness(float fitness);
	float getAcceptableFitness();
	void setFunction(benchmarkFunction benchmark);
	benchmarkFunction getFunction();
	void setInputs(int features);
	int getInputs();
	void setProblemSettingsFile(char* fileName);
	char* getProblemSettingsFile();
};
