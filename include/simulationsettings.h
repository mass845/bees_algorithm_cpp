/* ---------------------------------------------------- */
/* this class stores the parameters defining the		*/ 
/* experimental test. These parameters are defined by   */
/* the user in file simulationsettings.par.				*/
/* ---------------------------------------------------- */

#pragma once

class SimulationSettings
{
private:
	int trials;								/*repetitions of the algorithm for averaging purposes*/
	int iterations;							/*evolutionary steps*/
	char* resultsFile;						/*file where results are saved*/
	char* simulationSettingsFile;			/*file where learning parameters are stored*/
public:
	SimulationSettings(char* settingsFileName);
	~SimulationSettings(void);
	void readSettings();
	void setTrials(int nr);
	int getTrials();
	void setIterations(int nr);
	int getIterations();
	void setResultsFile(char* fileName);
	char* getResultsFile();
	void setSimulationSettingsFile(char* fileName);
	char* getSimulationSettingsFile();
};
