/* ---------------------------------------------------- */
/* this class is only used to store and pass the  three */
/* classes containing the parameters defining the Bees  */
/* Algorithm, the test run, and the benchmark problem.  */
/* ---------------------------------------------------- */

#pragma once
#include "problemsettings.h"
#include "simulationsettings.h"
#include "beesettings.h"

class Settings
{
private:
	BeeSettings* beeParameters;
	SimulationSettings* simulationParameters;
	ProblemSettings* testProblemSettings;
public:
	Settings(void);
	Settings(int n,	int e, int m,	int ne,	int nb,int cycles, float ngh);
	~Settings(void);
	void readSettings(char* beeSettingsFileName, char* testSettingsFileName, char* problemSettingsFileName);
	void setBeeParameters(BeeSettings* parameters);
	BeeSettings* getBeeParameters();
	void setSimulationParameters(SimulationSettings* parameters);
	SimulationSettings* getSimulationParameters();
	void setTestProblemSettings(ProblemSettings* parameters);
	ProblemSettings* getTestProblemSettings();
};
